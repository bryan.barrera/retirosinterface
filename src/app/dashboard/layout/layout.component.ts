import { Component, OnInit } from '@angular/core';
declare interface RouteInfo {
  path: string;
  title: string;
  rtlTitle: string;
  icon: string;
  class: string;
  hide: string;
}
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public titulo = 'Usuarios';
  public direction = 'icon-double-left';
  public ROUTES: RouteInfo[] = [

    {
      path: "reporting",
      title: "Reporting",
      rtlTitle: "Reporting",
      icon: "icon-single-02",
      class: "",
      hide: ""
    },
    {
      path: "rocketchat",
      title: "RocketChat",
      rtlTitle: "RocketChat",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    },
    {
      path: "wfm",
      title: "WFM",
      rtlTitle: "WFM",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    },
    {
      path: "maestro",
      title: "Maestro",
      rtlTitle: "Maestro",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    },{
      path: "financiera",
      title: "Financiera",
      rtlTitle: "Financiera",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    },{
      path: "nomina",
      title: "Nómina",
      rtlTitle: "RocketChat",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    },{
      path: "gestionhumana",
      title: "Gestión Humana",
      rtlTitle: "Gestión Humana",
      icon: "icon-cloud-upload-94",
      class: "",
      hide: "hidden"
    }
  ];
  public menuItems: any[] = [];
  constructor() { }

  ngOnInit(): void {
    $("#menu-toggle").click(function (e) {
      e.preventDefault();

      $("#wrapper").toggleClass("toggled");
    });
    this.menuItems = this.ROUTES;
  }

}
