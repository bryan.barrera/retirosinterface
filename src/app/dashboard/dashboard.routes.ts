import { Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

export const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'retiros', pathMatch: 'full' }
    ]
  }
];