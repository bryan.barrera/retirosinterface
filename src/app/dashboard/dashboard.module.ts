import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { RetirosComponent } from './retiros/retiros.component';
import { RouterModule } from '@angular/router';
import { dashboardRoutes } from './dashboard.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [LayoutComponent, RetirosComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(dashboardRoutes),
    FormsModule, 
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
